import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
@Injectable()
export class ProductsService {
  productsObservable;

  getProducts(){
    
    this.productsObservable = this.af.database.list('/products').map(
      products =>{
        products.map(
          product => {
            product.categoryName = [];
           
                 product.categoryName.push(
                this.af.database.object('/category/' + product.categoryId)
              )
                                                    
          }
        );
        return products;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
     return this.productsObservable;
 	}
  deleteProduct(product){
let productKey = product.$key;
this.af.database.object('/products/' + productKey).remove();
    
  }

  constructor(private af:AngularFire) { }

}
