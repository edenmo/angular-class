import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoice/invoice';
import {InvoicesService} from '../invoices/invoices.service'

import {NgForm} from '@angular/forms';
@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
 @Output() invoiceAddedEvent= new EventEmitter<Invoice>();
//@Output() invoiceAddedEvent= ;
invoice:Invoice = {name:'', amount:''};

onSubmit(form:NgForm){
console.log(form);


this.invoiceAddedEvent.emit(this.invoice);
this.invoice= {
  name:'',
  amount:''
 
}
// this._invoicesService.addinvoice(invoice);


}
  constructor() { }

  ngOnInit() {
  }

}
