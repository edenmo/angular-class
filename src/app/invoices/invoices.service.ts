import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {
 invoicesObservable;
 getInvoices(){
   /* let users = [
    {name:'Jhon',email:'jhon@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
    ]
    return users;*/
      //  return this._http.get(this._url).map(res =>res.json()).delay(1000)
 this.invoicesObservable = this.af.database.list('/invoices');
   return this.invoicesObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)
  }
    addinvoice(invoice){
    this.invoicesObservable.push(invoice);//push is a function on array, adds object in end of array.
}
  constructor(private af:AngularFire) { }

}
